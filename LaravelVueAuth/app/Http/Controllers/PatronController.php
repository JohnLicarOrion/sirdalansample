<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use Illuminate\Http\Request;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // if (request('search')) {
        //     return Patron::where('FirstName', 'like', '%' .  request('search') . '%')->latest()->get();
        // } else {
        //     return Patron::latest()->get();
        //     // return Patron::orderBy('id', 'desc')->paginate();
        // }

        // $Patron = Patron::query();
        // if (request('search')) {
        //     return  $Patron->where('FirstName', 'like', '%' .  request('search') . '%')->latest()->get();
        // } else {
        //     return $Patron->latest()->get();
        //     // return Patron::orderBy('id', 'desc')->paginate();
        // }

        return Patron::when(request('search'), function ($query) {
            $query->where('FirstName', 'like', '%' .  request('search') . '%')
                ->orWhere('LastName', 'like', '%' .  request('search') . '%')
                ->orWhere('Email', 'like', '%' .  request('search') . '%');
        })->latest()->paginate(8);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $PatronRequest)
    {
        $Patrons = Patron::create($PatronRequest->all());
        return response()->json(['message' => 'Job Posting Saved', 'data' => $Patrons]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Patron  $patron
     * @return \Illuminate\Http\Response
     */
    public function show(Patron $patron)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Patron  $patron
     * @return \Illuminate\Http\Response
     */
    public function edit(Patron $patron)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Patron  $patron
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $PatronRequest, Patron $Patron)
    {
        $Patron->update($PatronRequest->all());
        return response()->json(['message' => 'Data Updated', 'data' => $Patron]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Patron  $patron
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patron $Patron)
    {

        $Patron->delete();
        return response()->json(['message' => 'Data been Deleted']);
    }
}
